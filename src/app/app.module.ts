import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { ProjectsPage } from '../pages/projects/projects';
import { DashboardPage } from '../pages/dashboard/dashboard';
import { TeamsPage } from '../pages/teams/teams';
import { ProjectPage } from '../pages/project/project';
import { CreateprojectPage } from '../pages/createproject/createproject';
import { TeamPage } from '../pages/team/team';
import { CreateteamPage } from '../pages/createteam/createteam';
import { ListPage } from '../pages/list/list';
import { CreatelistPage } from '../pages/createlist/createlist';
import { TaskPage } from '../pages/task/task';
import { CreatetaskPage } from '../pages/createtask/createtask';
import { LoginPage } from '../pages/login/login';
import { RegisterPage } from '../pages/register/register';
import { ForgotpassPage } from '../pages/forgotpass/forgotpass';
import { AccountService } from '../services/account'
import { ResetpassPage } from '../pages/resetpass/resetpass';

import {HttpModule} from '@angular/http';
import { InvitePage } from "../pages/invite/invite";

@NgModule({
  declarations: [
    MyApp,
    ProjectsPage,
    DashboardPage,
    TeamsPage,
    ProjectPage,
    CreateprojectPage,
    TeamPage,
    CreateteamPage,
    ListPage,
    CreatelistPage,
    TaskPage,
    CreatetaskPage,
    LoginPage,
    RegisterPage,
    InvitePage,
    ForgotpassPage,
    ResetpassPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    ProjectsPage,
    DashboardPage,
    TeamsPage,
    ProjectPage,
    CreateprojectPage,
    TeamPage,
    CreateteamPage,
    ListPage,
    CreatelistPage,
    TaskPage,
    CreatetaskPage,
    LoginPage,
    RegisterPage,
    InvitePage,
    ForgotpassPage,
    ResetpassPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    AccountService,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
