import { Component, ViewChild } from '@angular/core';
import { Platform, NavController, MenuController, ModalController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { LoginPage } from '../pages/login/login';
import { RegisterPage } from '../pages/register/register';
import { InvitePage } from '../pages/invite/invite';
import { AccountService } from '../services/account';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  signedIn: boolean;
  rootPage:any = LoginPage;
  registerPage:any = RegisterPage;

  @ViewChild('sideMenuContent') nav: NavController;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen,
    private menuCtrl: MenuController,
    private modalCtrl: ModalController,
    private accSvc: AccountService) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
    this.signedIn = this.accSvc.isLoggedIn();
  }

  onLoad(page:any){
    this.nav.setRoot(page);
    this.menuCtrl.close();
  }

  inviteNewMember(){
    let modal = this.modalCtrl.create(InvitePage);
    modal.present();
    //console.log("asd");
  }

  isThisActive(){
    this.signedIn = this.accSvc.isLoggedIn();
    return this.signedIn;
  }

  logout(){
    this.accSvc.setActive(false);
    this.nav.setRoot(this.rootPage);
    this.menuCtrl.close();
  }

  signup(){
    this.nav.push(this.registerPage);
    this.menuCtrl.close();
  }
}

