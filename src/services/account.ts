export class AccountService {
    private isActive = false;

    setActive (isLogin: boolean) {
        this.isActive = isLogin;
    }
    isLoggedIn () {
        return this.isActive;
    }
}   