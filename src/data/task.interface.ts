export class T_Task {
    desc: string;
    checked: boolean;
    deadline: Date;

    constructor (desc: string, checked: boolean, deadline: Date) {
        this.desc = desc;
        this.checked = checked;
        this.deadline = deadline;
    }
}