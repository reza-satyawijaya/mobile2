import { T_List } from '../data/list.interface';

export class T_Project {
    name: string;
    ownerId: string;
    lists: T_List[];

    constructor (name: string, ownerId: string, lists: T_List[]) {
        this.name = name;
        this.ownerId = ownerId;
        this.lists = lists.slice();
    }
}