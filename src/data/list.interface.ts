import { T_Task } from '../data/task.interface';

export class T_List {
    name:string;
    tasks: T_Task[];

    constructor (name: string, tasks: T_Task[]) {
        this.name = name;
        this.tasks = tasks.slice();
    }
}