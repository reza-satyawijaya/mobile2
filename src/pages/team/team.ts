import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ProjectsPage } from '../projects/projects';

/**
 * Generated class for the TeamPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-team',
  templateUrl: 'team.html',
})
export class TeamPage {
  projectsPage: any;
  team: any;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.projectsPage = ProjectsPage;
    this.team = this.navParams.data.team;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TeamPage');
  }
  moveToProjects(){
    this.navCtrl.push(this.projectsPage, {
      owner: this.team
    });
  }
}
