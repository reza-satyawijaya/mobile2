import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { NgForm } from "@angular/forms/src/forms";

import {Http, Response, HttpModule, RequestOptions, Headers} from '@angular/http';

/**
 * Generated class for the InvitePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-invite',
  templateUrl: 'invite.html',
})
export class InvitePage {
  allMembers: string[] = ["ani", "budi", "siti","wwaw"];
  selectedMember: any = [];
  searchResults: string[] = [];
  addedMembers: string[] = [];
  constructor(public navCtrl: NavController, public navParams: NavParams, public http: Http) {
    var sessionToken = "J6sSRdTQft0l6QWpFj1opICVRtc6Q2drubolMWYuvUpvseGk6gg4XBpjrdLrL1B05ICs0eidw69GjFbMcXLnHAR2KAWexUcTBi0ZqsCjwTg2wHjKvUGsf8YlU0hqKgoCptqOxs45AchWWSB4kBOeMoITdTgsXYaqnkHBUEFqpSRlfwo7fpk89oTIEHfIerYmDvLjL5ATqv217WMYiicLOkWV8QkoizmxoX8nOi1tNCw1etKcO32j9c4D1JrqMP1";
    this.http.post('http://192.168.153.4:8000/api/user/retrieve', 
    {
      'sessionToken' : sessionToken
    }
  ).subscribe((response: Response) => {
    console.log(response);
    var temp = JSON.stringify(response);
    console.log(temp);
    
  });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad InvitePage');
  }

  search(e, form: NgForm){
    e.preventDefault();
    console.log("Search");
    this.searchResults = [];
    
    var searchQuery = form.value.txtSearch;
    console.log(searchQuery);

    var index, value;
    for (index = 0; index < this.allMembers.length; ++index) {
      // value = this.allMembers[index];
      if (this.allMembers[index].includes(searchQuery)) {
        // nemu yang sama kyk input
        // console.log(value);
        this.searchResults.push(this.allMembers[index]);
      }
    }
    console.log(this.searchResults);
  }

  isAdded(member : string) : boolean{
    let memberFind: string;
    memberFind = this.addedMembers.find((m : string, index : number, addedMembers : Array<string>) : boolean => {
      return member === m;
    });
    if(memberFind){
      return true;
    } else{
      return false;
    }
  }

  add(member: string) {
    console.log(member);
    this.addedMembers.push(member);
    console.log(this.addedMembers);
  }

  remove(member: string){
    let index = this.addedMembers.indexOf(member);
    this.addedMembers.splice(index,1);
  }

  isNotEmpty(){
    if(this.addedMembers && this.addedMembers.length){
      //not empty
      return true;
    } else{
      //empty
      return false;
    }
  }

  inviteMembers(){
    console.log("Invite");
    var url = "https://databases.000webhost.com/index.php";
    var method = "POST";
    var postData = this.addedMembers;
    var shouldBeAsync = true;
    
    var request = new XMLHttpRequest();

    request.onload = function () {

      var status = request.status; // HTTP response status, e.g., 200 for "200 OK"
      var data = request.responseText; // Returned data, e.g., an HTML document.
    }
    request.open(method, url, shouldBeAsync);
    
    request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    
    // Actually sends the request to the server.
    request.send(postData);

    
  }

}
