import { Component } from '@angular/core';
import { IonicPage, ToastController, NavController, NavParams } from 'ionic-angular';
import { ProjectPage } from '../project/project';
import { CreateprojectPage } from '../createproject/createproject';

import { T_Project } from '../../data/project.interface';
import { T_List } from "../../data/list.interface";
import { T_Task } from "../../data/task.interface";

/**
 * Generated class for the ProjectsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-projects',
  templateUrl: 'projects.html',
})
export class ProjectsPage {
  projectPage: any;
  createprojectPage: any;
  owner: any;
  loadProgress: any;
  projectList: T_Project[];
  constructor(public navCtrl: NavController, public toastCtrl: ToastController, public navParams: NavParams) {
    this.projectPage = ProjectPage;
    this.createprojectPage = CreateprojectPage;
    this.owner = this.navParams.data.owner; //the owner's name, User or Team
    this.projectList = new Array<T_Project>( //probably will be queried from DB with owner's ID as key
      new T_Project("Project A", this.owner, [
        new T_List("List A", [
          new T_Task("Task A", false, new Date(2017, 12, 1)),
          new T_Task("Task B", false, new Date(2017, 12, 1)),
          new T_Task("Task C", false, new Date(2017, 12, 1)),
        ]),
        new T_List("List B", [
          new T_Task("Task A", false, new Date(2017, 12, 1)),
          new T_Task("Task B", false, new Date(2017, 12, 1)),
          new T_Task("Task C", false, new Date(2017, 12, 1)),
        ]),
        new T_List("List C", [
          new T_Task("Task A", false, new Date(2017, 12, 1)),
          new T_Task("Task B", false, new Date(2017, 12, 1)),
          new T_Task("Task C", false, new Date(2017, 12, 1)),
        ]),
      ]),
      new T_Project("Project B", this.owner, [
        new T_List("List A", [
          new T_Task("Task A", false, new Date(2017, 12, 1)),
          new T_Task("Task B", false, new Date(2017, 12, 1)),
          new T_Task("Task C", false, new Date(2017, 12, 1)),
        ]),
        new T_List("List B", [
          new T_Task("Task A", false, new Date(2017, 12, 1)),
          new T_Task("Task B", false, new Date(2017, 12, 1)),
          new T_Task("Task C", false, new Date(2017, 12, 1)),
        ]),
        new T_List("List C", [
          new T_Task("Task A", false, new Date(2017, 12, 1)),
          new T_Task("Task B", false, new Date(2017, 12, 1)),
          new T_Task("Task C", false, new Date(2017, 12, 1)),
        ]),
      ]),
      new T_Project("Project C", this.owner, [
        new T_List("List A", [
          new T_Task("Task A", false, new Date(2017, 12, 1)),
          new T_Task("Task B", false, new Date(2017, 12, 1)),
          new T_Task("Task C", false, new Date(2017, 12, 1)),
        ]),
        new T_List("List B", [
          new T_Task("Task A", false, new Date(2017, 12, 1)),
          new T_Task("Task B", false, new Date(2017, 12, 1)),
          new T_Task("Task C", false, new Date(2017, 12, 1)),
        ]),
        new T_List("List C", [
          new T_Task("Task A", false, new Date(2017, 12, 1)),
          new T_Task("Task B", false, new Date(2017, 12, 1)),
          new T_Task("Task C", false, new Date(2017, 12, 1)),
        ]),
      ]),
    );
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProjectsPage');
  }
  deleteProject(p: any){
    let toast = this.toastCtrl.create({
      message: "delete " + p.name + "?",
      duration: 2000,
      position: "bottom"
    });
    toast.present();
  }
  copyProject(p: any){
    let toast = this.toastCtrl.create({
      message: "copy " + p.name + "?",
      duration: 2000,
      position: "bottom"
    });
    toast.present();
  }
  editProject(p: any){
    let toast = this.toastCtrl.create({
      message: "edit " + p.name + "?",
      duration: 2000,
      position: "bottom"
    });
    toast.present();
  }
  starProject(p: any){
    let toast = this.toastCtrl.create({
      message: "star " + p.name + "?",
      duration: 2000,
      position: "bottom"
    });
    toast.present();
  }
  subscribeProject(p: any){
    let toast = this.toastCtrl.create({
      message: "subscribe to " + p.name + "?",
      duration: 2000,
      position: "bottom"
    });
    toast.present();
  }
  moveToProject(p: any){
    this.navCtrl.push(this.projectPage,{
      project: p //go to project page carrying the whole information. probably only needs the project ID and query it there
    });
  }
  addProject(){
    let toast = this.toastCtrl.create({
      message: "create new project?",
      duration: 2000,
      position: "bottom"
    });
    toast.present();
  }
}
