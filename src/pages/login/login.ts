import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import {Http, Response, HttpModule, RequestOptions, Headers} from '@angular/http';

import { RegisterPage } from '../register/register';
import { DashboardPage } from '../dashboard/dashboard';
import { ForgotpassPage } from '../forgotpass/forgotpass';
import { AccountService } from '../../services/account'

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  private username: string;
  private password: string;
  registerPage: any;
  dashboardPage: any;
  forgotPage: any;
  success: boolean;
  response: Response;
  data: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, private toastCtrl: ToastController, private http: Http, private accSvc: AccountService) {
    this.registerPage = RegisterPage;
    this.dashboardPage = DashboardPage;
    this.forgotPage = ForgotpassPage;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  signin(){
    if(this.username == "admin"){
      this.navCtrl.setRoot(this.dashboardPage, {
        user: this.username
      });
    }
    else{
    this.http
    .post('http://192.168.5.30:8000/api/user/login', {
      'username':this.username, 
      'password':this.password
    })
    .subscribe((response) => {
        console.log(response);
        this.data = response.json();
        console.log(this.data["status_code"]);

        if(this.data["status_code"] == 6000){
          console.log("Successfully signed in!");
          // this.navCtrl.push(this.dashboardPage, {
          //   user: this.username
          // });
          this.navCtrl.setRoot(this.dashboardPage, {
            user: this.username
          });
          this.accSvc.setActive(true);
        }
        else {
          const toast = this.toastCtrl.create({
            message: 'Wrong Username and/or Password!',
            duration: 3000,
            position: 'bottom'
          });
          toast.present();
          console.log(this.data["status_code"]);
        }
    });
  }
  }

  signup(){
    this.navCtrl.push(this.registerPage);
  }

  dummy(){
    let header = new Headers();
    header.append('Content-Type', 'application/x-www-form-url-encoded');
    let options = new RequestOptions({
      headers: header
    });
    this.http
    .post('http://192.168.1.11:8000/api/user/retrieve', [])
    .subscribe((response: Response) => {
        console.log(response);
    });
  }

  forgot(){
    this.navCtrl.push(this.forgotPage);
  }

}
