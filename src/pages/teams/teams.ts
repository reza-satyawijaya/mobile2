import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { TeamPage } from '../team/team';
import { CreateteamPage } from '../createteam/createteam';

/**
 * Generated class for the TeamsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-teams',
  templateUrl: 'teams.html',
})
export class TeamsPage {
  teamPage: any;
  createteamPage: any;
  user: any;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.teamPage = TeamPage;
    this.createteamPage = CreateteamPage;
    this.user = this.navParams.data.user; //string containing the user's name only
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TeamsPage');
  }
  moveToTeam(t: String){
    this.navCtrl.push(this.teamPage, {
      team: t
    });
  }
  moveToCreateteam(){
    this.navCtrl.push(this.createteamPage, {
      user: this.user
    });
  }
}
