import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { CreatelistPage } from '../createlist/createlist';
import { ListPage } from '../list/list';

import { T_Project } from '../../data/project.interface';
import { T_List } from '../../data/list.interface';

/**
 * Generated class for the ProjectPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-project',
  templateUrl: 'project.html',
})
export class ProjectPage {
  createlistPage: any;
  listPage: any;
  project: any;
  projectList: T_Project;
  constructor(public navCtrl: NavController, public toastCtrl: ToastController, public navParams: NavParams) {
    this.createlistPage = CreatelistPage;
    this.listPage = ListPage;
    this.project = this.navParams.data.project; //probably will query DB for lists with this params (project ID)
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProjectPage');
  }
  moveToCreatelist(){
    this.navCtrl.push(this.createlistPage,{
      project: this.project //go to createlist page carrying project credentials
    });
  }
  moveToList(l: any){
    this.navCtrl.push(this.listPage,{
      list: l //go to list page carrying the whole list information. probably will only need list ID
    });
  }
  menuList(l: any){
    let toast = this.toastCtrl.create({
      message: "show " + l.name + " menu",
      duration: 2000,
      position: "bottom"
    });
    toast.present();
  }
  addList(){
    let toast = this.toastCtrl.create({
      message: "create new list",
      duration: 2000,
      position: "bottom"
    });
    toast.present();
  }
}
