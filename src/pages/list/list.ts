import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { TaskPage } from '../task/task';
import { CreatetaskPage } from '../createtask/createtask';

import { T_List } from '../../data/list.interface';
import { T_Task } from '../../data/task.interface';

/**
 * Generated class for the ListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-list',
  templateUrl: 'list.html',
})
export class ListPage {
  taskPage: any;
  createtaskPage: any;
  list: any;
  taskList: T_List;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.taskPage = TaskPage;
    this.createtaskPage = CreatetaskPage;
    this.list = this.navParams.data.list; //probably will query DB with list ID as key
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ListPage');
  }
  moveToTask(t: any){
    this.navCtrl.push(this.taskPage, {
      task: t //go to task page passing the whole info. probably only need task ID
    });
  }
  moveToCreatetask(){
    this.navCtrl.push(this.createtaskPage, {
      list: this.list //go to createtask page carrying the list credentials
    });
  }
}
