import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the CreatelistPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-createlist',
  templateUrl: 'createlist.html',
})
export class CreatelistPage {
  project: any;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.project = this.navParams.data.project;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CreatelistPage');
  }
  createNewList(){
    /*
      probably call SQL query to create new entry in the lists table
    */
  }
}
