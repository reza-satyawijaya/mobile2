import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the CreatetaskPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-createtask',
  templateUrl: 'createtask.html',
})
export class CreatetaskPage {
  list: any;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.list = this.navParams.data.list;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CreatetaskPage');
  }
  createNewTask(){
    /*
      probably call SQL query to create new entry in the tasks table
    */
  }
}
