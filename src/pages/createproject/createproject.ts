import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the CreateprojectPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-createproject',
  templateUrl: 'createproject.html',
})
export class CreateprojectPage {
  owner: any;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.owner = this.navParams.data.owner;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CreateprojectPage');
  }

  createNewProject(){
    /*
      probably call SQL query to create new entry in the projects table
    */
  }
}
