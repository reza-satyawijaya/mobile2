import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { ProjectsPage } from '../projects/projects';
import { TeamsPage } from '../teams/teams';

/**
 * Generated class for the DashboardPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-dashboard',
  templateUrl: 'dashboard.html',
})
export class DashboardPage {
  projectsPage: any;
  teamsPage: any;
  user: any;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.projectsPage = ProjectsPage;
    this.teamsPage = TeamsPage;
    this.user = this.navParams.data.user; //string containing the user's name only
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DashboardPage');
  }
  moveToProjects(){
    this.navCtrl.push(this.projectsPage, {
      owner: this.user //passes the user so the projects page can load personal projects created by the user
    });
  }
  moveToTeams(){
    this.navCtrl.push(this.teamsPage, {
      user: this.user //passes the user so the teams page can load teams with the user in its roster
    });
  }
}
