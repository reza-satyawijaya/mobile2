import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import {Http, Response, HttpModule, RequestOptions, Headers} from '@angular/http';

import { DashboardPage } from '../dashboard/dashboard';

/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {
  private fullname: string;
  private username: string;
  private email: string;
  private password: string;

  homePage: any;
  dashboardPage: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, private toastCtrl: ToastController, private http: Http) {
    this.dashboardPage = DashboardPage;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterPage');
  }

  signup(){
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if(!re.test(this.email)) {
      const toast = this.toastCtrl.create({
        message: 'Invalid E-mail!',
        duration: 3000,
        position: 'bottom'
      });
      toast.present();
      console.log("Invalid E-mail!");
    }
    else {
      this.http.post('http://192.168.5.30:8000/api/user/create', 
        {
          'fullname': this.fullname, 
          'username': this.username, 
          'email':this.email, 
          'password':this.password
        }
      ).subscribe((response: Response) => {
        console.log(response);
      });
      const toast = this.toastCtrl.create({
        message: 'Successfully signed up!',
        duration: 3000,
        position: 'bottom'
      });
      toast.present();
      console.log("Successfully signed up!");
      // this.http
      // .post('http://192.168.1.11:8000/api/user/retrieve', [this.fullname])
      // .subscribe((response: Response) => {
      //     console.log(response);
      // });
      this.navCtrl.pop();
    }
  }

}
