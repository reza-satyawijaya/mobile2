webpackJsonp([16],{

/***/ 104:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CreatelistPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the CreatelistPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var CreatelistPage = (function () {
    function CreatelistPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.project = this.navParams.data.project;
    }
    CreatelistPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad CreatelistPage');
    };
    CreatelistPage.prototype.createNewList = function () {
        /*
          probably call SQL query to create new entry in the lists table
        */
    };
    CreatelistPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-createlist',template:/*ion-inline-start:"D:\Mobile2\mobile2\src\pages\createlist\createlist.html"*/`<!--\n\n  Generated template for the CreatelistPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n  <ion-navbar>\n\n    <ion-title>{{project.name}}\'s createlist</ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content padding>\n\n  <p>This is the Create List page (can be replaced with Alert Controller, Modal, etc). You can create new, blank Lists for the Project here.</p>\n\n  <p>In the original Trello we only need a name to create the list</p>\n\n  <button ion-button block (click)="createNewList()">Create New List</button>\n\n</ion-content>\n\n`/*ion-inline-end:"D:\Mobile2\mobile2\src\pages\createlist\createlist.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */]])
    ], CreatelistPage);
    return CreatelistPage;
}());

//# sourceMappingURL=createlist.js.map

/***/ }),

/***/ 105:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CreateprojectPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the CreateprojectPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var CreateprojectPage = (function () {
    function CreateprojectPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.owner = this.navParams.data.owner;
    }
    CreateprojectPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad CreateprojectPage');
    };
    CreateprojectPage.prototype.createNewProject = function () {
        /*
          probably call SQL query to create new entry in the projects table
        */
    };
    CreateprojectPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-createproject',template:/*ion-inline-start:"D:\Mobile2\mobile2\src\pages\createproject\createproject.html"*/`<!--\n\n  Generated template for the CreateprojectPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n  <ion-navbar>\n\n    <ion-title>{{owner}}\'s createproject</ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content padding>\n\n    <p>This is the Create Project page (can be replaced with Alert Controller, Modal, etc). You can create new, blank Projects for you or your team here.</p>\n\n    <p>Trello\'s create new project description:</p>\n\n    <p>name</p>\n\n    <p>team or personal (combobox for the teams, with none as option for personal)</p>\n\n    <p>privacy (will we even implement this?)</p>\n\n    <p>Additional fields:</p>\n\n    <p>members</p>\n\n    <p>background (what)</p>\n\n    <p>settings (permissions, privacy, personal or team, thumbnails, etc)</p>\n\n    <p>Add more here...</p>\n\n    <button ion-button block (click)="createNewProject()">Create New Project</button>\n\n</ion-content>\n\n`/*ion-inline-end:"D:\Mobile2\mobile2\src\pages\createproject\createproject.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */]])
    ], CreateprojectPage);
    return CreateprojectPage;
}());

//# sourceMappingURL=createproject.js.map

/***/ }),

/***/ 106:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CreatetaskPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the CreatetaskPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var CreatetaskPage = (function () {
    function CreatetaskPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.list = this.navParams.data.list;
    }
    CreatetaskPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad CreatetaskPage');
    };
    CreatetaskPage.prototype.createNewTask = function () {
        /*
          probably call SQL query to create new entry in the tasks table
        */
    };
    CreatetaskPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-createtask',template:/*ion-inline-start:"D:\Mobile2\mobile2\src\pages\createtask\createtask.html"*/`<!--\n\n  Generated template for the CreatetaskPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n  <ion-navbar>\n\n    <ion-title>{{list.name}}\'s createtask</ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content padding>\n\n  <p>This is the Create Task page (can be replaced with Alert Controller, Modal, etc). You can create new, blank Tasks for the List here.</p>\n\n  <p>In the original trello we only need a name to create new tasks</p>\n\n  <button ion-button block (click)="createNewTask()">Create New Task</button>\n\n</ion-content>\n\n`/*ion-inline-end:"D:\Mobile2\mobile2\src\pages\createtask\createtask.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */]])
    ], CreatetaskPage);
    return CreatetaskPage;
}());

//# sourceMappingURL=createtask.js.map

/***/ }),

/***/ 107:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CreateteamPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the CreateteamPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var CreateteamPage = (function () {
    function CreateteamPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.user = this.navParams.data.user;
    }
    CreateteamPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad CreateteamPage');
    };
    CreateteamPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-createteam',template:/*ion-inline-start:"D:\Mobile2\mobile2\src\pages\createteam\createteam.html"*/`<!--\n\n  Generated template for the CreateteamPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n  <ion-navbar>\n\n    <ion-title>{{user}}\'s createteam</ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content padding>\n\n    <p>This is the Create Team page (can be replaced with Alert Controller, Modal, etc). You can create new, blank Teams and invite other members later.</p>\n\n    <button ion-button block>Create New Team</button>\n\n</ion-content>\n\n`/*ion-inline-end:"D:\Mobile2\mobile2\src\pages\createteam\createteam.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */]])
    ], CreateteamPage);
    return CreateteamPage;
}());

//# sourceMappingURL=createteam.js.map

/***/ }),

/***/ 108:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProjectPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__createlist_createlist__ = __webpack_require__(104);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__list_list__ = __webpack_require__(109);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the ProjectPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ProjectPage = (function () {
    function ProjectPage(navCtrl, toastCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.toastCtrl = toastCtrl;
        this.navParams = navParams;
        this.createlistPage = __WEBPACK_IMPORTED_MODULE_2__createlist_createlist__["a" /* CreatelistPage */];
        this.listPage = __WEBPACK_IMPORTED_MODULE_3__list_list__["a" /* ListPage */];
        this.project = this.navParams.data.project; //probably will query DB for lists with this params (project ID)
    }
    ProjectPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ProjectPage');
    };
    ProjectPage.prototype.moveToCreatelist = function () {
        this.navCtrl.push(this.createlistPage, {
            project: this.project //go to createlist page carrying project credentials
        });
    };
    ProjectPage.prototype.moveToList = function (l) {
        this.navCtrl.push(this.listPage, {
            list: l //go to list page carrying the whole list information. probably will only need list ID
        });
    };
    ProjectPage.prototype.menuList = function (l) {
        var toast = this.toastCtrl.create({
            message: "show " + l.name + " menu",
            duration: 2000,
            position: "bottom"
        });
        toast.present();
    };
    ProjectPage.prototype.addList = function () {
        var toast = this.toastCtrl.create({
            message: "create new list",
            duration: 2000,
            position: "bottom"
        });
        toast.present();
    };
    ProjectPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-project',template:/*ion-inline-start:"D:\Mobile2\mobile2\src\pages\project\project.html"*/`<!--\n\n  Generated template for the ProjectPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n  <ion-navbar>\n\n    <ion-title>{{project.name}}</ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n<ion-content no-padding>\n\n  <ion-grid>\n\n    <ion-row no-padding>\n\n      <ion-col col-6 *ngFor="let l of project.lists" no-padding>\n\n        <ion-card>\n\n          <ion-toolbar color="primary">\n\n            <ion-label color="light" start>{{l.name}}</ion-label>\n\n            <ion-buttons end>\n\n              <button ion-button icon-only primary small (click)="menuList(l)">\n\n                <ion-icon name="menu"></ion-icon>\n\n              </button>\n\n            </ion-buttons>\n\n          </ion-toolbar>\n\n          <ion-card-content (click)="moveToList(l)">\n\n            <p>3 tasks in due</p>\n\n            <p>1 task overdue</p>\n\n            <p>2 tasks finished</p>\n\n            <p>Nearest deadline: 05 Jan 2018</p>\n\n          </ion-card-content>\n\n        </ion-card>\n\n      </ion-col>\n\n    </ion-row>\n\n  </ion-grid>\n\n  <ion-fab bottom right>\n\n    <button ion-fab color="secondary" (click)="addList()"><ion-icon name="add"></ion-icon></button>\n\n  </ion-fab>\n\n</ion-content>`/*ion-inline-end:"D:\Mobile2\mobile2\src\pages\project\project.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* ToastController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */]])
    ], ProjectPage);
    return ProjectPage;
}());

//# sourceMappingURL=project.js.map

/***/ }),

/***/ 109:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ListPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__task_task__ = __webpack_require__(110);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__createtask_createtask__ = __webpack_require__(106);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the ListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ListPage = (function () {
    function ListPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.taskPage = __WEBPACK_IMPORTED_MODULE_2__task_task__["a" /* TaskPage */];
        this.createtaskPage = __WEBPACK_IMPORTED_MODULE_3__createtask_createtask__["a" /* CreatetaskPage */];
        this.list = this.navParams.data.list; //probably will query DB with list ID as key
    }
    ListPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ListPage');
    };
    ListPage.prototype.moveToTask = function (t) {
        this.navCtrl.push(this.taskPage, {
            task: t //go to task page passing the whole info. probably only need task ID
        });
    };
    ListPage.prototype.moveToCreatetask = function () {
        this.navCtrl.push(this.createtaskPage, {
            list: this.list //go to createtask page carrying the list credentials
        });
    };
    ListPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-list',template:/*ion-inline-start:"D:\Mobile2\mobile2\src\pages\list\list.html"*/`<!--\n\n  Generated template for the ListPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n  <ion-navbar>\n\n    <ion-title>{{list.name}}</ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content padding>\n\n  <p>This is the List page. Here you may display all tasks associated to this particular List, access them, and create new Tasks.</p>\n\n  <p>Here we can:</p>\n\n  <p>Move List (positioning only, this only matters for the original trello where "lists" are only groupings of "tasks" (cards))</p>\n\n  <p>Copy List (create exact copy with new name)</p>\n\n  <p>Add tasks</p>\n\n  <p>Bulk processing tasks (move all, etc)</p>\n\n  <p>Subscribe (idk idk idk)</p>\n\n  <p>also, <a href="https://forum.ionicframework.com/t/ionic-swipeable-tabs-are-now-here/84266">check this out</a> for list UI</p>\n\n  <ion-list>\n\n    <button ion-item *ngFor="let t of list.tasks" (click)="moveToTask(t)">{{t.desc}}</button>\n\n  </ion-list>\n\n  <button ion-button block (click)="moveToCreatetask()">Create New Task</button>\n\n</ion-content>\n\n`/*ion-inline-end:"D:\Mobile2\mobile2\src\pages\list\list.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */]])
    ], ListPage);
    return ListPage;
}());

//# sourceMappingURL=list.js.map

/***/ }),

/***/ 110:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TaskPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the TaskPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var TaskPage = (function () {
    function TaskPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.task = this.navParams.data.task; //probably will query DB with the task ID
    }
    TaskPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad TaskPage');
    };
    TaskPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-task',template:/*ion-inline-start:"D:\Mobile2\mobile2\src\pages\task\task.html"*/`<!--\n\n  Generated template for the TaskPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n  <ion-navbar>\n\n    <ion-title>{{task.desc}}</ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content padding>\n\n  <p>This is the Task page. Here you can do most of the features offered, such as create checklists, upload resources, etc.</p>\n\n  <p>{{task.desc}}</p>\n\n  <p>{{task.checked}}</p>\n\n  <p>{{task.deadline}}</p>\n\n  <p>Here\'s the fun part. We can:</p>\n\n  <p>add description</p>\n\n  <p>add comments</p>\n\n  <p>look at activities</p>\n\n  <p>assign members</p>\n\n  <p>create checklists</p>\n\n  <p>add due date (or none as default)</p>\n\n  <p>attach files</p>\n\n  <p>move, copy, etc</p>\n\n  <button ion-button block>Do stuff</button>\n\n</ion-content>\n\n`/*ion-inline-end:"D:\Mobile2\mobile2\src\pages\task\task.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */]])
    ], TaskPage);
    return TaskPage;
}());

//# sourceMappingURL=task.js.map

/***/ }),

/***/ 111:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TeamsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__team_team__ = __webpack_require__(112);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__createteam_createteam__ = __webpack_require__(107);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the TeamsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var TeamsPage = (function () {
    function TeamsPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.teamPage = __WEBPACK_IMPORTED_MODULE_2__team_team__["a" /* TeamPage */];
        this.createteamPage = __WEBPACK_IMPORTED_MODULE_3__createteam_createteam__["a" /* CreateteamPage */];
        this.user = this.navParams.data.user; //string containing the user's name only
    }
    TeamsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad TeamsPage');
    };
    TeamsPage.prototype.moveToTeam = function (t) {
        this.navCtrl.push(this.teamPage, {
            team: t
        });
    };
    TeamsPage.prototype.moveToCreateteam = function () {
        this.navCtrl.push(this.createteamPage, {
            user: this.user
        });
    };
    TeamsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-teams',template:/*ion-inline-start:"D:\Mobile2\mobile2\src\pages\teams\teams.html"*/`<!--\n\n  Generated template for the TeamsPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n  <ion-navbar>\n\n    <ion-title>{{user}}\'s teams</ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content padding>\n\n  <p>This is the teams page. You can see which Teams you are currently a part of and access their Team dashboards.</p>\n\n  <button ion-button block (click)="moveToTeam(\'Team A\')">Team A</button>\n\n  <button ion-button block (click)="moveToTeam(\'Team B\')">Team B</button>\n\n  <button ion-button block (click)="moveToTeam(\'Team C\')">Team C</button>\n\n  <button ion-button block (click)="moveToCreateteam(\'User X\')">Create New Team</button>\n\n</ion-content>\n\n`/*ion-inline-end:"D:\Mobile2\mobile2\src\pages\teams\teams.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */]])
    ], TeamsPage);
    return TeamsPage;
}());

//# sourceMappingURL=teams.js.map

/***/ }),

/***/ 112:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TeamPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__projects_projects__ = __webpack_require__(52);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the TeamPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var TeamPage = (function () {
    function TeamPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.projectsPage = __WEBPACK_IMPORTED_MODULE_2__projects_projects__["a" /* ProjectsPage */];
        this.team = this.navParams.data.team;
    }
    TeamPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad TeamPage');
    };
    TeamPage.prototype.moveToProjects = function () {
        this.navCtrl.push(this.projectsPage, {
            owner: this.team
        });
    };
    TeamPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-team',template:/*ion-inline-start:"D:\Mobile2\mobile2\src\pages\team\team.html"*/`<!--\n\n  Generated template for the TeamPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n  <ion-navbar>\n\n    <ion-title>{{team}}</ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content padding>\n\n  <p>This is the Team page. Much alike to a user\'s Dashboard page, you can access Team Projects here.</p>\n\n  <button ion-button block (click)="moveToProjects()">Team Projects</button>\n\n</ion-content>\n\n`/*ion-inline-end:"D:\Mobile2\mobile2\src\pages\team\team.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */]])
    ], TeamPage);
    return TeamPage;
}());

//# sourceMappingURL=team.js.map

/***/ }),

/***/ 113:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ForgotpassPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__(32);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the ForgotpassPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ForgotpassPage = (function () {
    function ForgotpassPage(navCtrl, navParams, toastCtrl, http) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.toastCtrl = toastCtrl;
        this.http = http;
    }
    ForgotpassPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ForgotpassPage');
    };
    ForgotpassPage.prototype.forgotpw = function () {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (!re.test(this.email)) {
            var toast = this.toastCtrl.create({
                message: 'Invalid E-mail!',
                duration: 3000,
                position: 'bottom'
            });
            toast.present();
            console.log("Invalid E-mail!");
        }
        else {
            var toast = this.toastCtrl.create({
                message: 'E-mail has been sent to ' + this.email + '!',
                duration: 3000,
                position: 'bottom'
            });
            toast.present();
            console.log("E-mail has been sent!");
            this.navCtrl.pop();
        }
    };
    ForgotpassPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-forgotpass',template:/*ion-inline-start:"D:\Mobile2\mobile2\src\pages\forgotpass\forgotpass.html"*/`<!--\n\n  Generated template for the ForgotpassPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n  <ion-navbar>\n\n    <ion-title>Forgot Password?</ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n<ion-content padding>\n\n  <form (ngSubmit)="forgotpw()">\n\n    <ion-item>\n\n      <ion-label fixed>Email</ion-label>\n\n      <ion-input  type="email"\n\n                  name="email"\n\n                  [(ngModel)]="email"\n\n                  placeholder="Your Email"\n\n                  required>\n\n      </ion-input>\n\n    </ion-item>\n\n    <button ion-button type="submit" block>Send E-mail</button>\n\n  </form>\n\n</ion-content>`/*ion-inline-end:"D:\Mobile2\mobile2\src\pages\forgotpass\forgotpass.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* ToastController */], __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Http */]])
    ], ForgotpassPage);
    return ForgotpassPage;
}());

//# sourceMappingURL=forgotpass.js.map

/***/ }),

/***/ 114:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return InvitePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__(32);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the InvitePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var InvitePage = (function () {
    function InvitePage(navCtrl, navParams, http) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.http = http;
        this.allMembers = ["ani", "budi", "siti", "wwaw"];
        this.selectedMember = [];
        this.searchResults = [];
        this.addedMembers = [];
        var sessionToken = "J6sSRdTQft0l6QWpFj1opICVRtc6Q2drubolMWYuvUpvseGk6gg4XBpjrdLrL1B05ICs0eidw69GjFbMcXLnHAR2KAWexUcTBi0ZqsCjwTg2wHjKvUGsf8YlU0hqKgoCptqOxs45AchWWSB4kBOeMoITdTgsXYaqnkHBUEFqpSRlfwo7fpk89oTIEHfIerYmDvLjL5ATqv217WMYiicLOkWV8QkoizmxoX8nOi1tNCw1etKcO32j9c4D1JrqMP1";
        this.http.post('http://192.168.153.4:8000/api/user/retrieve', {
            'sessionToken': sessionToken
        }).subscribe(function (response) {
            console.log(response);
            var temp = JSON.stringify(response);
            console.log(temp);
        });
    }
    InvitePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad InvitePage');
    };
    InvitePage.prototype.search = function (e, form) {
        e.preventDefault();
        console.log("Search");
        this.searchResults = [];
        var searchQuery = form.value.txtSearch;
        console.log(searchQuery);
        var index, value;
        for (index = 0; index < this.allMembers.length; ++index) {
            // value = this.allMembers[index];
            if (this.allMembers[index].includes(searchQuery)) {
                // nemu yang sama kyk input
                // console.log(value);
                this.searchResults.push(this.allMembers[index]);
            }
        }
        console.log(this.searchResults);
    };
    InvitePage.prototype.isAdded = function (member) {
        var memberFind;
        memberFind = this.addedMembers.find(function (m, index, addedMembers) {
            return member === m;
        });
        if (memberFind) {
            return true;
        }
        else {
            return false;
        }
    };
    InvitePage.prototype.add = function (member) {
        console.log(member);
        this.addedMembers.push(member);
        console.log(this.addedMembers);
    };
    InvitePage.prototype.remove = function (member) {
        var index = this.addedMembers.indexOf(member);
        this.addedMembers.splice(index, 1);
    };
    InvitePage.prototype.isNotEmpty = function () {
        if (this.addedMembers && this.addedMembers.length) {
            //not empty
            return true;
        }
        else {
            //empty
            return false;
        }
    };
    InvitePage.prototype.inviteMembers = function () {
        console.log("Invite");
        var url = "https://databases.000webhost.com/index.php";
        var method = "POST";
        var postData = this.addedMembers;
        var shouldBeAsync = true;
        var request = new XMLHttpRequest();
        request.onload = function () {
            var status = request.status; // HTTP response status, e.g., 200 for "200 OK"
            var data = request.responseText; // Returned data, e.g., an HTML document.
        };
        request.open(method, url, shouldBeAsync);
        request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        // Actually sends the request to the server.
        request.send(postData);
    };
    InvitePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-invite',template:/*ion-inline-start:"D:\Mobile2\mobile2\src\pages\invite\invite.html"*/`<!--\n\n  Generated template for the InvitePage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n  <ion-navbar>\n\n    <!-- <ion-buttons start>\n\n      <button ion-button menuToggle>\n\n        <ion-icon name="menu"></ion-icon>\n\n      </button>\n\n    </ion-buttons> -->\n\n    <ion-title>Invite</ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content padding>\n\n  <form #f="ngForm">\n\n    <ion-grid>\n\n      <ion-row>\n\n        <ion-col col-9>\n\n            <ion-input type="text" name="txtSearch" required ngModel \n\n              placeholder="Search by Email or Username..." ></ion-input>\n\n        </ion-col>\n\n        <ion-col col-3>\n\n          <button ion-button>\n\n            <ion-icon name="search" (click)="search($event, f)"></ion-icon>\n\n          </button>\n\n        </ion-col>\n\n      </ion-row>\n\n    </ion-grid>\n\n      \n\n    <ion-list id="resultList">\n\n      <!-- generates check list from search result here -->\n\n      <ion-item *ngFor="let result of searchResults; let i = index;">\n\n        <ion-grid>\n\n          <ion-row class="row">\n\n            <ion-col col-9><h3>{{searchResults[i]}}</h3></ion-col>\n\n            <ion-col col-3>\n\n              <!-- <button ion-button (click)="invite(searchResults[i])">add</button> -->\n\n              <button ion-button (click)="remove(searchResults[i])" color="danger" *ngIf="isAdded(searchResults[i])">\n\n                <ion-icon name="remove-circle"></ion-icon> \n\n              </button>\n\n              <button ion-button (click)="add(searchResults[i])" color="primary" *ngIf="!(isAdded(searchResults[i]))">\n\n                <ion-icon name="add-circle"></ion-icon>\n\n              </button>\n\n            </ion-col>\n\n          </ion-row>\n\n        </ion-grid>\n\n      </ion-item>\n\n    </ion-list>\n\n\n\n    <!-- generates name added to invite -->\n\n    <ion-list id="addedList">\n\n      <p *ngIf="isNotEmpty()">Send Invitations to...</p>\n\n      <ion-item *ngFor="let addedMember of addedMembers; let i = index;">\n\n        <ion-grid>\n\n          <ion-row>\n\n            <ion-col col-6><h3>{{addedMembers[i]}}</h3></ion-col>\n\n          </ion-row>\n\n        </ion-grid>\n\n      </ion-item>\n\n    </ion-list>\n\n\n\n    <button ion-button block type="submit" [disabled]="!(isNotEmpty())" (click)="inviteMembers()">Invite</button>  \n\n  </form>\n\n</ion-content>\n\n`/*ion-inline-end:"D:\Mobile2\mobile2\src\pages\invite\invite.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Http */]])
    ], InvitePage);
    return InvitePage;
}());

//# sourceMappingURL=invite.js.map

/***/ }),

/***/ 115:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__(32);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__register_register__ = __webpack_require__(53);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__dashboard_dashboard__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__forgotpass_forgotpass__ = __webpack_require__(113);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__services_account__ = __webpack_require__(81);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var LoginPage = (function () {
    function LoginPage(navCtrl, navParams, toastCtrl, http, accSvc) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.toastCtrl = toastCtrl;
        this.http = http;
        this.accSvc = accSvc;
        this.registerPage = __WEBPACK_IMPORTED_MODULE_3__register_register__["a" /* RegisterPage */];
        this.dashboardPage = __WEBPACK_IMPORTED_MODULE_4__dashboard_dashboard__["a" /* DashboardPage */];
        this.forgotPage = __WEBPACK_IMPORTED_MODULE_5__forgotpass_forgotpass__["a" /* ForgotpassPage */];
    }
    LoginPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad LoginPage');
    };
    LoginPage.prototype.signin = function () {
        var _this = this;
        if (this.username == "admin") {
            this.navCtrl.setRoot(this.dashboardPage, {
                user: this.username
            });
        }
        else {
            this.http
                .post('http://192.168.5.30:8000/api/user/login', {
                'username': this.username,
                'password': this.password
            })
                .subscribe(function (response) {
                console.log(response);
                _this.data = response.json();
                console.log(_this.data["status_code"]);
                if (_this.data["status_code"] == 6000) {
                    console.log("Successfully signed in!");
                    // this.navCtrl.push(this.dashboardPage, {
                    //   user: this.username
                    // });
                    _this.navCtrl.setRoot(_this.dashboardPage, {
                        user: _this.username
                    });
                    _this.accSvc.setActive(true);
                }
                else {
                    var toast = _this.toastCtrl.create({
                        message: 'Wrong Username and/or Password!',
                        duration: 3000,
                        position: 'bottom'
                    });
                    toast.present();
                    console.log(_this.data["status_code"]);
                }
            });
        }
    };
    LoginPage.prototype.signup = function () {
        this.navCtrl.push(this.registerPage);
    };
    LoginPage.prototype.dummy = function () {
        var header = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        header.append('Content-Type', 'application/x-www-form-url-encoded');
        var options = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["d" /* RequestOptions */]({
            headers: header
        });
        this.http
            .post('http://192.168.1.11:8000/api/user/retrieve', [])
            .subscribe(function (response) {
            console.log(response);
        });
    };
    LoginPage.prototype.forgot = function () {
        this.navCtrl.push(this.forgotPage);
    };
    LoginPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-login',template:/*ion-inline-start:"D:\Mobile2\mobile2\src\pages\login\login.html"*/`<!--\n\n  Generated template for the LoginPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n  <ion-navbar>\n\n    <ion-title>Login</ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content padding>\n\n  <form (ngSubmit)="signin()">\n\n    <ion-item>\n\n      <ion-label fixed>Username</ion-label>\n\n      <ion-input type="text" name="username" [(ngModel)]="username" placeholder="Your Username" required></ion-input>\n\n    </ion-item>\n\n    <ion-item>\n\n      <ion-label fixed>Password</ion-label>\n\n      <ion-input type="password" name="password" [(ngModel)]="password" placeholder="Your Password" required></ion-input>\n\n    </ion-item>\n\n    <button ion-button type="submit" block>Sign Me In</button>\n\n  </form>\n\n  <ion-label style="color:blue" (click)="signup()">Not a member yet? Register here!</ion-label>\n\n  <ion-label style="color:blue" (click)="forgot()">Forgot password</ion-label>\n\n</ion-content>\n\n`/*ion-inline-end:"D:\Mobile2\mobile2\src\pages\login\login.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* ToastController */], __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Http */], __WEBPACK_IMPORTED_MODULE_6__services_account__["a" /* AccountService */]])
    ], LoginPage);
    return LoginPage;
}());

//# sourceMappingURL=login.js.map

/***/ }),

/***/ 126:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 126;

/***/ }),

/***/ 167:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/createlist/createlist.module": [
		291,
		15
	],
	"../pages/createproject/createproject.module": [
		292,
		14
	],
	"../pages/createtask/createtask.module": [
		293,
		13
	],
	"../pages/createteam/createteam.module": [
		294,
		12
	],
	"../pages/dashboard/dashboard.module": [
		295,
		11
	],
	"../pages/forgotpass/forgotpass.module": [
		296,
		10
	],
	"../pages/invite/invite.module": [
		297,
		9
	],
	"../pages/list/list.module": [
		298,
		8
	],
	"../pages/login/login.module": [
		299,
		7
	],
	"../pages/project/project.module": [
		300,
		6
	],
	"../pages/projects/projects.module": [
		301,
		5
	],
	"../pages/register/register.module": [
		302,
		4
	],
	"../pages/resetpass/resetpass.module": [
		303,
		3
	],
	"../pages/task/task.module": [
		304,
		2
	],
	"../pages/team/team.module": [
		305,
		1
	],
	"../pages/teams/teams.module": [
		306,
		0
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 167;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 211:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ResetpassPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__(32);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the ResetpassPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ResetpassPage = (function () {
    function ResetpassPage(navCtrl, navParams, toastCtrl, http) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.toastCtrl = toastCtrl;
        this.http = http;
    }
    ResetpassPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ResetpassPage');
    };
    ResetpassPage.prototype.resetpw = function () {
        var toast = this.toastCtrl.create({
            message: 'Password has been changed!',
            duration: 3000,
            position: 'bottom'
        });
        toast.present();
        console.log("Password has been changed!");
        this.navCtrl.pop();
    };
    ResetpassPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-resetpass',template:/*ion-inline-start:"D:\Mobile2\mobile2\src\pages\resetpass\resetpass.html"*/`<!--\n\n  Generated template for the ResetpassPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n  <ion-navbar>\n\n    <ion-title>Reset Password</ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content padding>\n\n  <form (ngSubmit)="resetpw()">\n\n    <ion-item>\n\n      <ion-label fixed>Password</ion-label>\n\n      <ion-input type="password" name="newpass" [(ngModel)]="newpass" placeholder="Your New Password" required></ion-input>\n\n      <ion-input type="password" name="confirmpass" [(ngModel)]="confirmpass" placeholder="Confirm New Password" required></ion-input>\n\n    </ion-item>\n\n    <button ion-button type="submit" block>Reset Password</button>\n\n  </form>\n\n</ion-content>\n\n`/*ion-inline-end:"D:\Mobile2\mobile2\src\pages\resetpass\resetpass.html"*/,
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* ToastController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* ToastController */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Http */]) === "function" && _d || Object])
    ], ResetpassPage);
    return ResetpassPage;
    var _a, _b, _c, _d;
}());

//# sourceMappingURL=resetpass.js.map

/***/ }),

/***/ 212:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(213);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(236);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 236:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(207);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__ = __webpack_require__(210);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__app_component__ = __webpack_require__(290);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_projects_projects__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_dashboard_dashboard__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_teams_teams__ = __webpack_require__(111);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_project_project__ = __webpack_require__(108);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_createproject_createproject__ = __webpack_require__(105);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_team_team__ = __webpack_require__(112);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_createteam_createteam__ = __webpack_require__(107);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_list_list__ = __webpack_require__(109);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__pages_createlist_createlist__ = __webpack_require__(104);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__pages_task_task__ = __webpack_require__(110);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__pages_createtask_createtask__ = __webpack_require__(106);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__pages_login_login__ = __webpack_require__(115);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__pages_register_register__ = __webpack_require__(53);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__pages_forgotpass_forgotpass__ = __webpack_require__(113);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__services_account__ = __webpack_require__(81);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__pages_resetpass_resetpass__ = __webpack_require__(211);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__angular_http__ = __webpack_require__(32);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__pages_invite_invite__ = __webpack_require__(114);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
























var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_6__pages_projects_projects__["a" /* ProjectsPage */],
                __WEBPACK_IMPORTED_MODULE_7__pages_dashboard_dashboard__["a" /* DashboardPage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_teams_teams__["a" /* TeamsPage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_project_project__["a" /* ProjectPage */],
                __WEBPACK_IMPORTED_MODULE_10__pages_createproject_createproject__["a" /* CreateprojectPage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_team_team__["a" /* TeamPage */],
                __WEBPACK_IMPORTED_MODULE_12__pages_createteam_createteam__["a" /* CreateteamPage */],
                __WEBPACK_IMPORTED_MODULE_13__pages_list_list__["a" /* ListPage */],
                __WEBPACK_IMPORTED_MODULE_14__pages_createlist_createlist__["a" /* CreatelistPage */],
                __WEBPACK_IMPORTED_MODULE_15__pages_task_task__["a" /* TaskPage */],
                __WEBPACK_IMPORTED_MODULE_16__pages_createtask_createtask__["a" /* CreatetaskPage */],
                __WEBPACK_IMPORTED_MODULE_17__pages_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_18__pages_register_register__["a" /* RegisterPage */],
                __WEBPACK_IMPORTED_MODULE_23__pages_invite_invite__["a" /* InvitePage */],
                __WEBPACK_IMPORTED_MODULE_19__pages_forgotpass_forgotpass__["a" /* ForgotpassPage */],
                __WEBPACK_IMPORTED_MODULE_21__pages_resetpass_resetpass__["a" /* ResetpassPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* MyApp */], {}, {
                    links: [
                        { loadChildren: '../pages/createlist/createlist.module#CreatelistPageModule', name: 'CreatelistPage', segment: 'createlist', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/createproject/createproject.module#CreateprojectPageModule', name: 'CreateprojectPage', segment: 'createproject', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/createtask/createtask.module#CreatetaskPageModule', name: 'CreatetaskPage', segment: 'createtask', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/createteam/createteam.module#CreateteamPageModule', name: 'CreateteamPage', segment: 'createteam', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/dashboard/dashboard.module#DashboardPageModule', name: 'DashboardPage', segment: 'dashboard', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/forgotpass/forgotpass.module#ForgotpassPageModule', name: 'ForgotpassPage', segment: 'forgotpass', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/invite/invite.module#InvitePageModule', name: 'InvitePage', segment: 'invite', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/list/list.module#ListPageModule', name: 'ListPage', segment: 'list', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/login/login.module#LoginPageModule', name: 'LoginPage', segment: 'login', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/project/project.module#ProjectPageModule', name: 'ProjectPage', segment: 'project', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/projects/projects.module#ProjectsPageModule', name: 'ProjectsPage', segment: 'projects', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/register/register.module#RegisterPageModule', name: 'RegisterPage', segment: 'register', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/resetpass/resetpass.module#ResetpassPageModule', name: 'ResetpassPage', segment: 'resetpass', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/task/task.module#TaskPageModule', name: 'TaskPage', segment: 'task', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/team/team.module#TeamPageModule', name: 'TeamPage', segment: 'team', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/teams/teams.module#TeamsPageModule', name: 'TeamsPage', segment: 'teams', priority: 'low', defaultHistory: [] }
                    ]
                }),
                __WEBPACK_IMPORTED_MODULE_22__angular_http__["c" /* HttpModule */]
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["a" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_6__pages_projects_projects__["a" /* ProjectsPage */],
                __WEBPACK_IMPORTED_MODULE_7__pages_dashboard_dashboard__["a" /* DashboardPage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_teams_teams__["a" /* TeamsPage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_project_project__["a" /* ProjectPage */],
                __WEBPACK_IMPORTED_MODULE_10__pages_createproject_createproject__["a" /* CreateprojectPage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_team_team__["a" /* TeamPage */],
                __WEBPACK_IMPORTED_MODULE_12__pages_createteam_createteam__["a" /* CreateteamPage */],
                __WEBPACK_IMPORTED_MODULE_13__pages_list_list__["a" /* ListPage */],
                __WEBPACK_IMPORTED_MODULE_14__pages_createlist_createlist__["a" /* CreatelistPage */],
                __WEBPACK_IMPORTED_MODULE_15__pages_task_task__["a" /* TaskPage */],
                __WEBPACK_IMPORTED_MODULE_16__pages_createtask_createtask__["a" /* CreatetaskPage */],
                __WEBPACK_IMPORTED_MODULE_17__pages_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_18__pages_register_register__["a" /* RegisterPage */],
                __WEBPACK_IMPORTED_MODULE_23__pages_invite_invite__["a" /* InvitePage */],
                __WEBPACK_IMPORTED_MODULE_19__pages_forgotpass_forgotpass__["a" /* ForgotpassPage */],
                __WEBPACK_IMPORTED_MODULE_21__pages_resetpass_resetpass__["a" /* ResetpassPage */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
                __WEBPACK_IMPORTED_MODULE_20__services_account__["a" /* AccountService */],
                { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["u" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* IonicErrorHandler */] }
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 262:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return T_Project; });
var T_Project = (function () {
    function T_Project(name, ownerId, lists) {
        this.name = name;
        this.ownerId = ownerId;
        this.lists = lists.slice();
    }
    return T_Project;
}());

//# sourceMappingURL=project.interface.js.map

/***/ }),

/***/ 263:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return T_List; });
var T_List = (function () {
    function T_List(name, tasks) {
        this.name = name;
        this.tasks = tasks.slice();
    }
    return T_List;
}());

//# sourceMappingURL=list.interface.js.map

/***/ }),

/***/ 264:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return T_Task; });
var T_Task = (function () {
    function T_Task(desc, checked, deadline) {
        this.desc = desc;
        this.checked = checked;
        this.deadline = deadline;
    }
    return T_Task;
}());

//# sourceMappingURL=task.interface.js.map

/***/ }),

/***/ 290:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(210);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(207);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_login_login__ = __webpack_require__(115);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_register_register__ = __webpack_require__(53);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_invite_invite__ = __webpack_require__(114);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__services_account__ = __webpack_require__(81);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var MyApp = (function () {
    function MyApp(platform, statusBar, splashScreen, menuCtrl, modalCtrl, accSvc) {
        this.menuCtrl = menuCtrl;
        this.modalCtrl = modalCtrl;
        this.accSvc = accSvc;
        this.rootPage = __WEBPACK_IMPORTED_MODULE_4__pages_login_login__["a" /* LoginPage */];
        this.registerPage = __WEBPACK_IMPORTED_MODULE_5__pages_register_register__["a" /* RegisterPage */];
        platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            statusBar.styleDefault();
            splashScreen.hide();
        });
        this.signedIn = this.accSvc.isLoggedIn();
    }
    MyApp.prototype.onLoad = function (page) {
        this.nav.setRoot(page);
        this.menuCtrl.close();
    };
    MyApp.prototype.inviteNewMember = function () {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_6__pages_invite_invite__["a" /* InvitePage */]);
        modal.present();
        //console.log("asd");
    };
    MyApp.prototype.isThisActive = function () {
        this.signedIn = this.accSvc.isLoggedIn();
        return this.signedIn;
    };
    MyApp.prototype.logout = function () {
        this.accSvc.setActive(false);
        this.nav.setRoot(this.rootPage);
        this.menuCtrl.close();
    };
    MyApp.prototype.signup = function () {
        this.nav.push(this.registerPage);
        this.menuCtrl.close();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('sideMenuContent'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */])
    ], MyApp.prototype, "nav", void 0);
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"D:\Mobile2\mobile2\src\app\app.html"*/`<ion-menu [content]="sideMenuContent">\n\n    <ion-header>\n\n        <ion-toolbar>Menu</ion-toolbar>\n\n    </ion-header>\n\n    <ion-content>\n\n        <ion-list>\n\n            <button ion-item *ngIf="isThisActive()">\n\n                <ion-icon name="contacts" item-left></ion-icon>\n\n                Members\n\n            </button>\n\n        </ion-list>\n\n        <ion-list>\n\n            <button ion-item (click)="inviteNewMember()" *ngIf="isThisActive()">\n\n                <ion-icon name="person-add" item-left></ion-icon>\n\n                Invite\n\n            </button>\n\n        </ion-list>\n\n        <ion-list>\n\n            <button ion-item (click)="logout()" *ngIf="isThisActive()">\n\n                <ion-icon name="log-out" item-left></ion-icon>\n\n                Logout\n\n            </button>\n\n        </ion-list>\n\n        <ion-list>\n\n            <button ion-item (click)="signup()" *ngIf="!isThisActive()">\n\n                <ion-icon name="log-in" item-left></ion-icon>\n\n                Sign Up\n\n            </button>\n\n        </ion-list>\n\n    </ion-content>\n\n</ion-menu>\n\n\n\n<ion-nav [root]="rootPage" #sideMenuContent></ion-nav>\n\n`/*ion-inline-end:"D:\Mobile2\mobile2\src\app\app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Platform */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* MenuController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_7__services_account__["a" /* AccountService */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 51:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DashboardPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__projects_projects__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__teams_teams__ = __webpack_require__(111);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the DashboardPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var DashboardPage = (function () {
    function DashboardPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.projectsPage = __WEBPACK_IMPORTED_MODULE_2__projects_projects__["a" /* ProjectsPage */];
        this.teamsPage = __WEBPACK_IMPORTED_MODULE_3__teams_teams__["a" /* TeamsPage */];
        this.user = this.navParams.data.user; //string containing the user's name only
    }
    DashboardPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad DashboardPage');
    };
    DashboardPage.prototype.moveToProjects = function () {
        this.navCtrl.push(this.projectsPage, {
            owner: this.user //passes the user so the projects page can load personal projects created by the user
        });
    };
    DashboardPage.prototype.moveToTeams = function () {
        this.navCtrl.push(this.teamsPage, {
            user: this.user //passes the user so the teams page can load teams with the user in its roster
        });
    };
    DashboardPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-dashboard',template:/*ion-inline-start:"D:\Mobile2\mobile2\src\pages\dashboard\dashboard.html"*/`<!--\n\n  Generated template for the DashboardPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n  <ion-navbar>\n\n    <ion-title>{{user}}\'s dashboard</ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content padding>\n\n  <p>This will be the dashboard of the logged account. You can check your personal projects and your teams here.</p>\n\n  <button ion-button block (click)="moveToProjects()">Personal Projects</button>\n\n  <button ion-button block (click)="moveToTeams()">Teams</button>\n\n</ion-content>\n\n`/*ion-inline-end:"D:\Mobile2\mobile2\src\pages\dashboard\dashboard.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */]])
    ], DashboardPage);
    return DashboardPage;
}());

//# sourceMappingURL=dashboard.js.map

/***/ }),

/***/ 52:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProjectsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__project_project__ = __webpack_require__(108);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__createproject_createproject__ = __webpack_require__(105);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__data_project_interface__ = __webpack_require__(262);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__data_list_interface__ = __webpack_require__(263);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__data_task_interface__ = __webpack_require__(264);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







/**
 * Generated class for the ProjectsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ProjectsPage = (function () {
    function ProjectsPage(navCtrl, toastCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.toastCtrl = toastCtrl;
        this.navParams = navParams;
        this.projectPage = __WEBPACK_IMPORTED_MODULE_2__project_project__["a" /* ProjectPage */];
        this.createprojectPage = __WEBPACK_IMPORTED_MODULE_3__createproject_createproject__["a" /* CreateprojectPage */];
        this.owner = this.navParams.data.owner; //the owner's name, User or Team
        this.projectList = new Array(//probably will be queried from DB with owner's ID as key
        new __WEBPACK_IMPORTED_MODULE_4__data_project_interface__["a" /* T_Project */]("Project A", this.owner, [
            new __WEBPACK_IMPORTED_MODULE_5__data_list_interface__["a" /* T_List */]("List A", [
                new __WEBPACK_IMPORTED_MODULE_6__data_task_interface__["a" /* T_Task */]("Task A", false, new Date(2017, 12, 1)),
                new __WEBPACK_IMPORTED_MODULE_6__data_task_interface__["a" /* T_Task */]("Task B", false, new Date(2017, 12, 1)),
                new __WEBPACK_IMPORTED_MODULE_6__data_task_interface__["a" /* T_Task */]("Task C", false, new Date(2017, 12, 1)),
            ]),
            new __WEBPACK_IMPORTED_MODULE_5__data_list_interface__["a" /* T_List */]("List B", [
                new __WEBPACK_IMPORTED_MODULE_6__data_task_interface__["a" /* T_Task */]("Task A", false, new Date(2017, 12, 1)),
                new __WEBPACK_IMPORTED_MODULE_6__data_task_interface__["a" /* T_Task */]("Task B", false, new Date(2017, 12, 1)),
                new __WEBPACK_IMPORTED_MODULE_6__data_task_interface__["a" /* T_Task */]("Task C", false, new Date(2017, 12, 1)),
            ]),
            new __WEBPACK_IMPORTED_MODULE_5__data_list_interface__["a" /* T_List */]("List C", [
                new __WEBPACK_IMPORTED_MODULE_6__data_task_interface__["a" /* T_Task */]("Task A", false, new Date(2017, 12, 1)),
                new __WEBPACK_IMPORTED_MODULE_6__data_task_interface__["a" /* T_Task */]("Task B", false, new Date(2017, 12, 1)),
                new __WEBPACK_IMPORTED_MODULE_6__data_task_interface__["a" /* T_Task */]("Task C", false, new Date(2017, 12, 1)),
            ]),
        ]), new __WEBPACK_IMPORTED_MODULE_4__data_project_interface__["a" /* T_Project */]("Project B", this.owner, [
            new __WEBPACK_IMPORTED_MODULE_5__data_list_interface__["a" /* T_List */]("List A", [
                new __WEBPACK_IMPORTED_MODULE_6__data_task_interface__["a" /* T_Task */]("Task A", false, new Date(2017, 12, 1)),
                new __WEBPACK_IMPORTED_MODULE_6__data_task_interface__["a" /* T_Task */]("Task B", false, new Date(2017, 12, 1)),
                new __WEBPACK_IMPORTED_MODULE_6__data_task_interface__["a" /* T_Task */]("Task C", false, new Date(2017, 12, 1)),
            ]),
            new __WEBPACK_IMPORTED_MODULE_5__data_list_interface__["a" /* T_List */]("List B", [
                new __WEBPACK_IMPORTED_MODULE_6__data_task_interface__["a" /* T_Task */]("Task A", false, new Date(2017, 12, 1)),
                new __WEBPACK_IMPORTED_MODULE_6__data_task_interface__["a" /* T_Task */]("Task B", false, new Date(2017, 12, 1)),
                new __WEBPACK_IMPORTED_MODULE_6__data_task_interface__["a" /* T_Task */]("Task C", false, new Date(2017, 12, 1)),
            ]),
            new __WEBPACK_IMPORTED_MODULE_5__data_list_interface__["a" /* T_List */]("List C", [
                new __WEBPACK_IMPORTED_MODULE_6__data_task_interface__["a" /* T_Task */]("Task A", false, new Date(2017, 12, 1)),
                new __WEBPACK_IMPORTED_MODULE_6__data_task_interface__["a" /* T_Task */]("Task B", false, new Date(2017, 12, 1)),
                new __WEBPACK_IMPORTED_MODULE_6__data_task_interface__["a" /* T_Task */]("Task C", false, new Date(2017, 12, 1)),
            ]),
        ]), new __WEBPACK_IMPORTED_MODULE_4__data_project_interface__["a" /* T_Project */]("Project C", this.owner, [
            new __WEBPACK_IMPORTED_MODULE_5__data_list_interface__["a" /* T_List */]("List A", [
                new __WEBPACK_IMPORTED_MODULE_6__data_task_interface__["a" /* T_Task */]("Task A", false, new Date(2017, 12, 1)),
                new __WEBPACK_IMPORTED_MODULE_6__data_task_interface__["a" /* T_Task */]("Task B", false, new Date(2017, 12, 1)),
                new __WEBPACK_IMPORTED_MODULE_6__data_task_interface__["a" /* T_Task */]("Task C", false, new Date(2017, 12, 1)),
            ]),
            new __WEBPACK_IMPORTED_MODULE_5__data_list_interface__["a" /* T_List */]("List B", [
                new __WEBPACK_IMPORTED_MODULE_6__data_task_interface__["a" /* T_Task */]("Task A", false, new Date(2017, 12, 1)),
                new __WEBPACK_IMPORTED_MODULE_6__data_task_interface__["a" /* T_Task */]("Task B", false, new Date(2017, 12, 1)),
                new __WEBPACK_IMPORTED_MODULE_6__data_task_interface__["a" /* T_Task */]("Task C", false, new Date(2017, 12, 1)),
            ]),
            new __WEBPACK_IMPORTED_MODULE_5__data_list_interface__["a" /* T_List */]("List C", [
                new __WEBPACK_IMPORTED_MODULE_6__data_task_interface__["a" /* T_Task */]("Task A", false, new Date(2017, 12, 1)),
                new __WEBPACK_IMPORTED_MODULE_6__data_task_interface__["a" /* T_Task */]("Task B", false, new Date(2017, 12, 1)),
                new __WEBPACK_IMPORTED_MODULE_6__data_task_interface__["a" /* T_Task */]("Task C", false, new Date(2017, 12, 1)),
            ]),
        ]));
    }
    ProjectsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ProjectsPage');
    };
    ProjectsPage.prototype.deleteProject = function (p) {
        var toast = this.toastCtrl.create({
            message: "delete " + p.name + "?",
            duration: 2000,
            position: "bottom"
        });
        toast.present();
    };
    ProjectsPage.prototype.copyProject = function (p) {
        var toast = this.toastCtrl.create({
            message: "copy " + p.name + "?",
            duration: 2000,
            position: "bottom"
        });
        toast.present();
    };
    ProjectsPage.prototype.editProject = function (p) {
        var toast = this.toastCtrl.create({
            message: "edit " + p.name + "?",
            duration: 2000,
            position: "bottom"
        });
        toast.present();
    };
    ProjectsPage.prototype.starProject = function (p) {
        var toast = this.toastCtrl.create({
            message: "star " + p.name + "?",
            duration: 2000,
            position: "bottom"
        });
        toast.present();
    };
    ProjectsPage.prototype.subscribeProject = function (p) {
        var toast = this.toastCtrl.create({
            message: "subscribe to " + p.name + "?",
            duration: 2000,
            position: "bottom"
        });
        toast.present();
    };
    ProjectsPage.prototype.moveToProject = function (p) {
        this.navCtrl.push(this.projectPage, {
            project: p //go to project page carrying the whole information. probably only needs the project ID and query it there
        });
    };
    ProjectsPage.prototype.addProject = function () {
        var toast = this.toastCtrl.create({
            message: "create new project?",
            duration: 2000,
            position: "bottom"
        });
        toast.present();
    };
    ProjectsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-projects',template:/*ion-inline-start:"D:\Mobile2\mobile2\src\pages\projects\projects.html"*/`<!--\n\n  Generated template for the ProjectsPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n  <ion-navbar>\n\n    <ion-title>{{owner}}\'s projects</ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content no-padding>\n\n  <ion-grid>\n\n    <ion-row no-padding>\n\n      <ion-col col-6 *ngFor="let p of projectList" no-padding>\n\n        <ion-card>\n\n          <ion-toolbar color="primary">\n\n            <ion-badge item-start color="danger">19</ion-badge>\n\n            <ion-buttons end>\n\n              <button ion-button icon-only primary small (click)="subscribeProject(p)">\n\n                <ion-icon name="notifications-outline"></ion-icon>\n\n              </button>\n\n              <button ion-button icon-only primary small (click)="starProject(p)">\n\n                <ion-icon name="star-outline"></ion-icon>\n\n              </button>\n\n            </ion-buttons>\n\n          </ion-toolbar>\n\n          <ion-card-content (click)="moveToProject(p)">\n\n            <ion-item>\n\n              <ion-avatar item-start>\n\n                <img src="http://lorempixel.com/200/200" width="10%">\n\n              </ion-avatar>\n\n              <h2>{{p.name}}</h2>\n\n              <p>Owner: Tedja</p>\n\n              <p>3 Members</p>\n\n            </ion-item>\n\n            <ion-label><p>Last activity: 2 days ago</p></ion-label>\n\n          </ion-card-content>\n\n          <ion-segment>\n\n              <ion-segment-button (click)="deleteProject(p)">\n\n                <ion-icon name="trash"></ion-icon>\n\n              </ion-segment-button>\n\n              <ion-segment-button (click)="copyProject(p)">\n\n                  <ion-icon name="copy"></ion-icon>\n\n              </ion-segment-button>\n\n              <ion-segment-button (click)="editProject(p)">\n\n                  <ion-icon name="create"></ion-icon>\n\n              </ion-segment-button>\n\n          </ion-segment>\n\n        </ion-card>\n\n      </ion-col>\n\n    </ion-row>\n\n  </ion-grid>\n\n  <ion-fab bottom right>\n\n    <button ion-fab color="secondary" (click)="addProject()"><ion-icon name="add"></ion-icon></button>\n\n  </ion-fab>\n\n</ion-content>\n\n`/*ion-inline-end:"D:\Mobile2\mobile2\src\pages\projects\projects.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* ToastController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */]])
    ], ProjectsPage);
    return ProjectsPage;
}());

//# sourceMappingURL=projects.js.map

/***/ }),

/***/ 53:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RegisterPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__(32);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__dashboard_dashboard__ = __webpack_require__(51);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var RegisterPage = (function () {
    function RegisterPage(navCtrl, navParams, toastCtrl, http) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.toastCtrl = toastCtrl;
        this.http = http;
        this.dashboardPage = __WEBPACK_IMPORTED_MODULE_3__dashboard_dashboard__["a" /* DashboardPage */];
    }
    RegisterPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad RegisterPage');
    };
    RegisterPage.prototype.signup = function () {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (!re.test(this.email)) {
            var toast = this.toastCtrl.create({
                message: 'Invalid E-mail!',
                duration: 3000,
                position: 'bottom'
            });
            toast.present();
            console.log("Invalid E-mail!");
        }
        else {
            this.http.post('http://192.168.5.30:8000/api/user/create', {
                'fullname': this.fullname,
                'username': this.username,
                'email': this.email,
                'password': this.password
            }).subscribe(function (response) {
                console.log(response);
            });
            var toast = this.toastCtrl.create({
                message: 'Successfully signed up!',
                duration: 3000,
                position: 'bottom'
            });
            toast.present();
            console.log("Successfully signed up!");
            // this.http
            // .post('http://192.168.1.11:8000/api/user/retrieve', [this.fullname])
            // .subscribe((response: Response) => {
            //     console.log(response);
            // });
            this.navCtrl.pop();
        }
    };
    RegisterPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-register',template:/*ion-inline-start:"D:\Mobile2\mobile2\src\pages\register\register.html"*/`<!--\n\n  Generated template for the RegisterPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n  <ion-navbar>\n\n    <ion-title>Register</ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content padding>\n\n  <form (ngSubmit)="signup()">\n\n    <ion-item>\n\n      <ion-label fixed>Full Name</ion-label>\n\n      <ion-input type="text" name="fullname" [(ngModel)]="fullname" placeholder="Your Full Name" required></ion-input>\n\n    </ion-item>\n\n    <ion-item>\n\n      <ion-label fixed>Username</ion-label>\n\n      <ion-input type="text" name="username" [(ngModel)]="username" placeholder="Your Username" required></ion-input>\n\n    </ion-item>\n\n    <ion-item>\n\n      <ion-label fixed>Email</ion-label>\n\n      <ion-input type="text" name="email" [(ngModel)]="email" placeholder="Your Email" required></ion-input>\n\n    </ion-item>\n\n    <ion-item>\n\n      <ion-label fixed>Password</ion-label>\n\n      <ion-input type="password" name="password" [(ngModel)]="password" placeholder="Your Password" required></ion-input>\n\n    </ion-item>\n\n    <button ion-button type="submit" block>Sign Me Up</button>\n\n  </form>\n\n</ion-content>\n\n`/*ion-inline-end:"D:\Mobile2\mobile2\src\pages\register\register.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* ToastController */], __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Http */]])
    ], RegisterPage);
    return RegisterPage;
}());

//# sourceMappingURL=register.js.map

/***/ }),

/***/ 81:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AccountService; });
var AccountService = (function () {
    function AccountService() {
        this.isActive = false;
    }
    AccountService.prototype.setActive = function (isLogin) {
        this.isActive = isLogin;
    };
    AccountService.prototype.isLoggedIn = function () {
        return this.isActive;
    };
    return AccountService;
}());

//# sourceMappingURL=account.js.map

/***/ })

},[212]);
//# sourceMappingURL=main.js.map